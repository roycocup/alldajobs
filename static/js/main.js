

/*
* Changes the css stylesheet is the window falls 
* below a specific width
*/
function screenSizeCss(){
	var reset = "/static/css/small.css";
	var main = "/static/css/main.css";
	var width = $(window).width();
	
	if (width <= 600) {
		$("link#mainStyle").attr({href:reset});
	} else {
		$("link#mainStyle").attr({href:main});
	}
}


function tabs(){
	//when clicked
	$('#tabs a li').click(function(){
		var source = $(this).html();
		//hide all 
		$('.content').removeClass('visible').addClass('invisible');
		$('#tabs a li').removeClass('tabSelect');
		//show the one selected
		$('.content.'+ source).removeClass('invisible').addClass('visible');
		$('#tabs a li#'+ source).addClass('tabSelect');
	});
}

//jobs to execute after page is loaded
$(document).ready(function(){
	tabs(); //make the tabs buttons work
	screenSizeCss()//check that the starting screen is not too small
});

//on resize
$(window).resize(function(){
	screenSizeCss();
});