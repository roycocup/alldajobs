var first = 0;
var speed = 3000;
var pause = 5000;

function removeFirst(){
        var first = $('ul#listticker li:first').html();
        $('ul#listticker li:first .boxText')
                .animate({opacity: 0}, speed);

        $('ul#listticker li:first .boxImage')
                .css("position","relative")
                .animate({opacity: 0}, speed, function(){
                        $('ul#listticker li:first').animate({opacity:0}, speed).remove();
                });
        addLast(first);
}

function addLast(first){
        var last = first+'';
        $('ul#listticker').append(last)
        $('ul#listticker li:last .boxText')
                .animate({opacity: 1}, speed)
                .fadeIn('slow');

        $('ul#listticker li:last .boxImage')
                .animate({opacity: 1}, speed);
}

var interval = setInterval(removeFirst, pause);




