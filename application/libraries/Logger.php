<?php

/**
 * IO model to log files
 *
 * @author rodrigodias
 */
class Logger{
	
	
	public $fileName = "log.txt";
	
	public function __construct(){
		//return $this;
	}
	
	
	public function log($text, $level=null){
		$level = ($level) ? $level : "Info";
		$text = "Level - ". $text;
		$this->_write($text);
	}
	
	public function logVisitor($aTerms = null){
		$ip = $this->getIP();
		
		if (!empty($aTerms)){
			$text = "Visitor ip: ". $ip ." on ". date("d-m-Y H:i:s", $_SERVER['REQUEST_TIME'])
				." requesting skill:{$aTerms['skill']} where:{$aTerms['location']}"
				."\r\n";  
		}else {
			$text = "Visitor ip: ". $_SERVER['SERVER_ADDR']." on ". date("d-m-Y H:i:s", $_SERVER['REQUEST_TIME'])."\r\n";  
		}
		$this->_write($text, $this->fileName);
	}
	
	
	private function _write($text, $filename){
		file_put_contents($filename,$text,FILE_APPEND);
	}
	
	
	public function getIP() { 
		if ($_SERVER["REMOTE_ADDR"]) {
			$ip = $_SERVER["REMOTE_ADDR"]; 
		} else if(getenv("HTTP_X_FORWARDED_FOR")) {
			$ip = getenv("HTTP_X_FORWARDED_FOR"); 
		} else if(getenv("HTTP_CLIENT_IP")) {
			$ip = getenv("HTTP_CLIENT_IP");
		} else {
			$ip = "UNKNOWN";
		} 
		return $ip; 
	}
	
}


?>