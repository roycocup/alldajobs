<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Main extends CI_Controller {

        public function index() {
				$this->logger->logVisitor();
                $this->load->model("model_sources");
                $this->load->view('main');
        }
        
}

