<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jobs extends CI_Controller {

    public function all_da_jobs() {
        $data = array();
        //load the model we are going to use
        $this->load->model("model_sources");
        $this->load->model("model_cache");


        //if nothing has been sent
        if (!$this->input->post('skill') || !$this->input->post('location')) {
            $this->load->view('main', $data);
            return true;
        }
		
		//log this query
		$this->logger->logVisitor(array("skill"=>$this->input->post('skill'), "location"=>$this->input->post('location')));
		
        //get the posts into the params for the model
        $params = array(
            'skill'		=> $this->input->post('skill'),
            'location'	=> $this->input->post('location'),
            'jobTitle'	=> $this->input->post('skill'),
            'selected_sources' => array('gumtree', 'monster', "jobsite", "trovit")
        );

        //check if its cached
        if (CACHE_ON)
            $info = $this->model_cache->getCached($params);    

        //call the model if the cache is not there
        if (@!$info)    
            $info = $this->model_sources->get($params);
		
        

        //if we don't get anything from the model
        //then we have NO RESULTS or false
        if (@!$info) {
            $data['results'] = false;
        } else {
            //sign the results to ok
            $data['results'] = true;
            //cache it
            if (CACHE_ON)
                $this->model_cache->cacheResults($params, $info);
        }

        //show the results from the model
        $this->showResults($data, $info);
    }

    private function showResults($data, $info) {
        //sort results
        $info = $this->sortResults($info);

        $data['content'] = $info;
        $this->load->view('main', $data);
    }


    private function sortResults($data){
        return $data;
    }

    

}

