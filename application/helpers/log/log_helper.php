<?php
	
function logme($message, $severity = null)
{
	include_once 'logClass.php';
	
	if (!$severity) $severity = Log::SEVERITY_INFO;
	$date		= date('Y-m-d H:i:s')." - ";
	$severity	= $severity. " - ";
	$payload	= $date.$severity.$message;

	$logger = new Log();
	$logger->log($payload);
	return true;
}

?>