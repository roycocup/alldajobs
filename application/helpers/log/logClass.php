<?php

class Log
{

	const SEVERITY_DEBUG	= 'Debug';
	const SEVERITY_INFO		= 'Info';
	const SEVERITY_NOTICE	= 'Notice';
	const SEVERITY_FATAL	= 'Fatal Error';
	
	public $fileName = 'ErrorLog';
	public $filePath;
	
	public function __construct()
	{
		$this->filePath = APPPATH."logs/";
		$this->fileName = $this->_getFileName();
	}
	
	private function _getFileName()
	{
		$month	= date('m');
		$year	= date('Y');
		$week	= date('W');
		$fileName = $this->fileName."-".$year."-".$month."-".$week.".log";
		return $fileName;
	}

	
	private function _write($payload)
	{
		if($this->_canWriteToFolder($this->filePath))
		{
			$fullFilePath = $this->_getFullFilePath();
			file_put_contents($fullFilePath, $payload."\r\n", FILE_APPEND);
		}
	}
	
	private function _canWriteToFolder($directory)
	{
		if (!is_dir($directory))
		{
			if(!mkdir($directory, 0777, true)){
				return false;
			}
		} 
		return true;
	}
	
	
	private function _getFullFilePath()
	{	
		if(!is_file($this->fileName))
		{
			touch($this->filePath.$this->fileName); 
		}
		return $this->filePath.$this->fileName;
	}
	
	public function log($payload)
	{
		$this->_write($payload);
		return true;
	}
}

?>