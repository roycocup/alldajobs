<?php

function getGmapsUrl($address, $city, $width='', $height=''){
	if (!$width || !$height) {$width=300; $height=400;}
	$gaddress = "http://maps.google.com/maps/api/staticmap?center=";
	$gaddress .= urlencode($address);
	$gaddress .= urlencode(",".$city);
	$gaddress .= "&zoom=16";
	$gaddress .= "&size={$width}x{$height}";
	$gaddress .= "&markers=color:green|label:|".$address.",".$city;
	$gaddress .= "&sensor=true";
	return $gaddress;
}

function getGeopoints($address){
	$mainString = 'http://maps.googleapis.com/maps/api/geocode/';
	$responseType = 'json?';
	$address = urlencode($address);
	$sensor = 'false';
	$request = $mainString.$responseType.'address='.$address.'&sensor='.$sensor;
	//TODO: replace this with curl if php.ini blocks file_get_contents
	$response = file_get_contents($request);
	/*
	 * $geoPoints = json_decode(getGeopoints('5, paymal house, stepney green, London, England'));
	 * print_r($geoPoints->results[0]->geometry->location);
	 */
	return $response;
}

?>