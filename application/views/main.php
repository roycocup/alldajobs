<?php $this->load->view('partials/header'); ?>

<div id="wrapper">
    <div id="content">
            <?php
            if (@$content) {
                //only show advertising when we have some results
                $this->load->view('partials/leftcolumn');
                //no results flow
                if (!$results) {
                    echo "<div>No job posts found for your search</div>";
                } else {
					echo '<ul id="tabs">';
                    foreach ($content as $source => $item) { 
						echo "<a href='#'><li id=\"$source\" class='tabs'>$source</li></a>";
					}
					echo "</ul>";
                    foreach ($content as $source => $content) {
						echo "<div class=\"content $source invisible \">";
                        foreach ($content as $k => $item) {
                            echo "<div class='listbox'>";
                            echo '<div class="logo"><img class="listsourceImage" src="/static/images/' . $source . '_logo.png"/ alt="'.$source.'"></div>';
                            echo "<div style='clear:both'></div>";
                            echo "<div class='title'>" . $item['title'] . "</div>";
                            if (@$item['pubdate'])
                                echo "</br><div class='pubdate'>". $item['pubdate'] . "</div>";
                            if (@$item['description'])
                                echo "<div class='description'>". $item['description'] . "</div>";
                            echo "<div class='link'> <a href='" . $item['link'] . "' target='_blank'>Open</a></div>";
                            echo "</div>";
                        }
						echo "</div>";
                    }
                }
            }
            ?>
    </div>
</div>

<script> 
	//show the first one
	function showFirstTab(source){
		$('#tabs li#'+source).addClass('tabSelect')
		$('.content.'+source).removeClass('invisible').addClass('visible');
	}
	showFirstTab('gumtree');
</script>
<?php $this->load->view('partials/footer'); ?>

