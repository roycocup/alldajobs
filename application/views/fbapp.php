<?php $this->load->view('partials/headerFB'); ?>

<div id="wrapper">
    <div id="content">
            <?php
            if (@$content) {
                //only show advertising when we have some results
                $this->load->view('partials/leftcolumn');
                //no results flow
                if (!$results) {
                    echo "<div>No job posts found for your search</div>";
                } else {
                    foreach ($content as $source => $content) {
                        if ($source == "results")continue;
                        foreach ($content as $k => $item) {
                            echo "<div class='listbox'>";
                            echo '<div class="logo"><img class="listsourceImage" src="/static/images/' . $source . '_logo.png"/ alt="'.$source.'"></div>';
                            echo "<div style='clear:both'></div>";
                            echo "<div class='title'>" . $item['title'] . "</div>";
                            if (@$item['pubdate'])
                                echo "</br><div class='pubdate'>". $item['pubdate'] . "</div>";
                            if (@$item['description'])
                                echo "<div class='description'>". $item['description'] . "</div>";
                            echo "<div class='link'> <a href='" . $item['link'] . "' target='_blank'>Open</a></div>";
                            echo "</div>";
                        }
                    }
                }
            }
            ?>
    </div>
</div>

<?php $this->load->view('partials/footer'); ?>

