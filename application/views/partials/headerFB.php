<!DOCTYPE html>

<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Alldajobs" />
    
    <meta name="description" content="Job search for UK jobs, job adverts from websites all over the web, if the job is out there its in here,
          a list of jobs posted in the best job sites such as monster gumtree jobsite and trovit and posted here as a
          digest collection." />
    <meta name="keywords" content="jobs, uk jobs, jobsite, monster, gumtree, trovit, jobs in uk, digest of jobs, collection of jobs" />
    

    <script type="text/javascript" src="/static/js/jquery16.js"></script>
    <script type="text/javascript" src="/static/js/jquery-ui18.js"></script>
    <script type="text/javascript" src="/static/js/jquery.cycle.js"></script>
    <script type="text/javascript" src="/static/js/jquery.corner.js"></script>
    <script type="text/javascript" src="/static/js/main.js"></script>
    <script type="text/javascript" src="/static/js/boxanim.js"></script>
    <!--<script type="text/javascript" src="/static/js/mootools-1.4.0.1.js"></script>-->    
    
	<?php echo link_tag('static/css/fb.css'); ?>

    <title>Alldajobs</title>

    
</head>
<body>

    <div id="container">

            <div id="header">
                <div id="logo"><img src="/static/images/alldajobs.png" alt="logo"/></div>
                <div style="clear: both"></div>
                <div id="form">
                        <?php
                            //form
                            echo form_open('jobs/all_da_jobs', array('name'=>'form', 'class'=>'form'));
                                    echo "What ".form_input(array("name"=>"skill", "size"=>"10", "class"=>'inputtext'));
                                    echo "Where ".form_input(array("name"=>"location", "size"=>"10", "class"=>'inputtext'));
                                    echo '<button name="" type="button" class="inputsubmit" >Go!</button>';
                            echo form_close();

                            //Show the search params
                            if (@$this->input->post('skill') && @$this->input->post('location')){
                                    echo '<div style="clear: both"></div>';
                                    echo "<div id='searchHeader'>";
                                    echo "Searches for jobs in ";
                                    echo "<strong>".$this->input->post('location')."</strong>";
                                    echo " with ";
                                    echo "<strong>".$this->input->post('skill')."</strong>";
                                    echo "</div>";
                            }
                        ?>

                </div>
                <div style="clear: both"></div>
            </div>
<script>
        $(document).ready(function(){
                $(".form .inputtext")[0].focus();
                //corners
                $(".form .inputsubmit").corner("15px");
                //keypress on form
                $(".form").keydown(function(event){
                        if (event.which == '13') {
                                $("form").submit();
                        }
                });

                //click on form's button'
                $(".form .inputsubmit").click(function(){
                        $("form").submit();
                });

                //open link
                $(".link a").click(function(){
                    $(this).addClass('seenlink');
                    $(this).html("Open again").parents("div.listbox").css("background-color", "#3CACEF");
                    //append a mark? Its probably better to have it there first and hide it
                    //$(this).parents("div.listbox").append("<div id='seenMark'>here</div>");
                });




        });
</script>




            
