<!DOCTYPE html>

<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Alldajobs" />
    
    <meta name="description" content="Job search for UK jobs, job adverts from websites all over the web, if the job is out there its in here,
          a list of jobs posted in the best job sites such as monster gumtree jobsite and trovit and posted here as a
          digest collection." />
    <meta name="keywords" content="jobs, uk jobs, jobsite, monster, gumtree, trovit, jobs in uk, digest of jobs, collection of jobs" />
    
    <script type="text/javascript" src="/static/js/modernizr.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/static/js/jquery.corner.js"></script>
    <script type="text/javascript" src="/static/js/main.js"></script>
        
    <!--<link href='http://fonts.googleapis.com/css?family=League+Script|Montez|Annie+Use+Your+Telescope|Vibur' rel='stylesheet' type='text/css'>-->
	<link id="mainStyle" rel="stylesheet" href="/static/css/main.css" type="text/css" />

    <title>Alldajobs</title>

    
</head>
<body>

    <div id="container">

            <div id="header">
                <div id="logo"><img src="/static/images/alldajobs.png" alt="logo"/></div>
                <div id="headertext">Job adverts in the UK from websites all over the web. If the job is out there, we grab it.
          We look at the best job sites such as Monster, Gumtree, Jobsite and Trovit, and repost them here as a
          digest collection for your convenience.</div>
                <div style="clear: both"></div>
                <div id="form">
                        <?php
                            //form
                            echo form_open('jobs/all_da_jobs', array('name'=>'form', 'class'=>'form'));
                                    echo "<span id='what'>What</span> ".form_input(array("name"=>"skill", "size"=>"20", "class"=>'inputtext'));
                                    echo "<span id='where'>Where</span> ".form_input(array("name"=>"location", "size"=>"20", "class"=>'inputtext'));
                                    echo '<button name="" type="button" class="inputsubmit" >Go!</button>';
                            echo form_close();

                            //Show the search params
                            if (@$this->input->post('skill') && @$this->input->post('location')){
                                    echo '<div style="clear: both"></div>';
                                    echo "<div id='searchHeader'>";
                                    echo "Searches for jobs in ";
                                    echo "<strong>".$this->input->post('location')."</strong>";
                                    echo " with ";
                                    echo "<strong>".$this->input->post('skill')."</strong>";
                                    echo "</div>";
                            }
                        ?>

                </div>
                <div style="clear: both"></div>
            </div>
<script>
        $(document).ready(function(){
                $(".form .inputtext")[0].focus();
                //corners
                $(".form .inputsubmit").corner("15px");
                //keypress on form
                $(".form").keydown(function(event){
                        if (event.which == '13') {
                                $("form").submit();
                        }
                });

                //click on form's button'
                $(".form .inputsubmit").click(function(){
                        $("form").submit();
                });

                //open link
                $(".link a").click(function(){
                    $(this).addClass('seenlink');
                    $(this).html("Open again").parents("div.listbox").css("background-color", "#3CACEF");
                    //append a mark? Its probably better to have it there first and hide it
                    //$(this).parents("div.listbox").append("<div id='seenMark'>here</div>");
                });




        });
</script>




            
