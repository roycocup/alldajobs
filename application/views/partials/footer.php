
        <div style="clear: both"></div>
        
        <div id="footer">
            <p>@Copyright <?php echo date('Y', time());?> <a href="http://softroad.co.uk" style="text-decoration: none; color: white;">Softroad.co.uk</a>. All rights reserved</p>
            <hr>
            <p><a href="mailto:info@alldajobs.co.uk">Contact us</a> | <a href="#">About Alldajobs.com</a>
                | <a href="#">Privacy Policy</a> | <a href="/tandc">Terms of use</a></p>
        </div>
</div>
</body>

<!-- Google Analytics -->

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26746569-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</html>
