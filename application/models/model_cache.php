<?php

class model_cache extends CI_Model {


    public function cacheResults($params, $data){
        $filename = $this->makeCacheFilename($params);
        //check existing cache
        $fileExists = $this->checkExistingCache($params);
        //create cache if it does not exist
        if (!$fileExists){
            file_put_contents("cache/".$filename.".cch", json_encode($data));
        }
        return true;
    }

    private function makeCacheFilename($params){
        $filename = md5($params['skill'].$params['location']);
        return $filename;
    }

    private function makeCacheTime(){
        $time = date('y/m/d h',time());
        return $time;
    }

    public function getCached($params){
        $cacheExists = $this->checkExistingCache($params);
        if (!$cacheExists) return false;
        $filename = $this->makeCacheFilename($params);
        $file = "cache/".$filename.".cch";
        $file_contents = json_decode(file_get_contents($file), true);
        return $file_contents;
    }

    public function checkExistingCache($params){
        $time = time();
        $filename = $this->makeCacheFilename($params);
        $file = "cache/".$filename.".cch";
        //if file does exist
        if (file_exists($file)){
            //what time was it created
            $timeCreated = filectime($file);
            if ($timeCreated < strtotime('30 minutes ago')){
                //destroy the cache file
                unlink($file);
                //yes, create a new cache file
                return false;
            }
            //file exists and is on timeframe
            return true;
        }
        //if file does not exist or was old
        return false;
        

    }
    
}


?>
