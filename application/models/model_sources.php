<?php

class model_sources extends CI_Model {

    public $skill;
    public $jobTitle;
    public $location;

    public $agencyKeywords = array("
                client,
                they"
    );
    public $sources = array(
        'gumtree',
        'trovit',
        'jobsite',
        'monster'
    );

    public function get($params) {
		
		//Sanitize params
		$params = cleanUpMyString($params);
		
        //get the parameters
        $this->skill				= $params['skill'];
        $this->jobTitle				= $params['jobTitle'];
        $this->location				= $params['location'];
        $this->selected_sources		= $params['selected_sources'];

        $params['url_jobsite'] = "http://www.jobsite.co.uk/cgi-bin/advsearch?rss_feed=1&skill_include={$params['skill']}&job_title_include={$params['jobTitle']}&location_include={$params['location']}&location_within=20&reqd_salary=ANY|&search_currency_code=GBP&search_single_currency_flag=N&search_salary_type=A&search_salary_low=ANY&daysback=15&scc=UK&compare_resolved=CO_LONDON&compare_search=London&jobtype=E";
        $params['url_gumtree'] = "http://www.gumtree.com/rssfeed/jobs/{$params['location']}/{$params['skill']}/page1";
        $params['url_monster'] = "http://rss.jobsearch.monster.com/rssquery.ashx?q={$params['skill']}%20{$params['location']}&rad_units=mi&cy=UK&pp=15&sort=dt.rv.di&baseurl=jobview.monster.co.uk";
        $params['url_trovit']  = "http://jobs.trovit.co.uk/index.php/cod.rss_jobs/what_d.{$params['skill']}%20{$params['location']}/";

        foreach ($this->sources as $source_name) {
            //only include the sources that we want.
            if (!in_array($source_name, $this->selected_sources)) continue;
            //include the source name in the params so we can
            //then pick the correct url
            $params['source_name'] = $source_name;
            //and go.
            $xml = $this->getAll($params);
            //if we get a false just keep the loop
            if (!$xml) {
				logme("source: ". $params['source_name']. " didn't come back with results or is having a fit");
				continue;
			}
            //do last minute checks
            $ok = $this->dataChecks($xml);
            if ($ok) $list[$source_name] = $xml;
        }
        if ($xml){
            return $list;
        }
    }


	/**
	 * Gets the xml information for one channel (source)
	 * in simpleXml object format
	 * @param type $params
	 * @return boolean 
	 */
     public function getAll($params) {
        $xml = $this->getXml($params);
        $items = array();
        $i = 1;
		
        if (!$xml)
            return false;

        //collect information from the source's xml
        foreach ($xml->channel->item as $item) {
			
			//fix for gumtree that has started to go stupid
			//if (preg_match('/gumtree/i', (string)$xml->channel->title, $match)) $item->link = substr_replace((string)$item->link, '', 1,22);
			
            $items[$i]['title']                         = word_limiter((string)$item->title, 50);
            $items[$i]['description']                   = word_limiter((string)$item->description, 50);
            $items[$i]['link']                          = (string)$item->link;
            if ($item->guid) $items[$i]['guid']         = (string)$item->guid;
            if ($item->pubDate){
                $pubdate = new DateTime($item->pubDate);
                $items[$i]['pubdate']                   = $pubdate->format('d M Y h:i');
            }
            $i++;
        }
        return $items;
    }

    private function getXml($params) {
        //TODO: check if xml cache already exists
        $xml = $this->fetchXml($params);
        return $xml;
    }

    private function fetchXml($params) {
        $url_source = "url_".$params['source_name'];
        $url = $params[$url_source];
        $xml = file_get_contents($url);
        //error handling
        if (!stripos($xml, "rss")) return false;
        if (stripos($xml, "No job postings found")) return false;

        //get this into an xml object
		//$xmlObj = new SimpleXMLElement($xml);
		$xmlObj = simplexml_load_string($xml);
		if ($xmlObj){
			return $xmlObj;
		} else {
			logme("Could not create simple xml object from ".$params['source_name']);
			return false;
		}
		
		
		
    }

    /**
     * Checks if the data actually has the words the
     * user requested
     * @return <bool>
     */
    private function dataChecks(){
        //check that keywords are really there
        return true;
    }

}

?>
